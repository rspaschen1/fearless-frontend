import React, {useState, useEffect} from 'react';

export default function AttendeeList() {
  const [attendees, setAttendees] = useState([]);
  const [sortBy, setSortBy] = useState("name");
  const [sortOrder, setSortOrder] = useState("asc");

  const getData = async () => {
    const response = await fetch("http://localhost:8001/api/attendees/");
    if (response.ok) {
      const data = await response.json();
      const attendees = data.attendees;
      setAttendees(attendees);
    }
  }

  useEffect(() => {
    getData();
  }, []);

  const handleSort = (sortBy) => {
    setSortBy(sortBy);
    setSortOrder((sortOrder === "asc") ? "desc" : "asc");
  };

  const sortedAttendees = attendees.sort((a, b) => {
    if (sortBy === "name") {
      if (sortOrder === "asc") {
        return a.name.localeCompare(b.name);
      } else {
        return b.name.localeCompare(a.name);
      }
    } else if (sortBy === "conference") {
      if (sortOrder === "asc") {
        return a.conference.localeCompare(b.conference);
      } else {
        return b.conference.localeCompare(a.conference);
      }
    }
  });

  return (
    <div className="container mt-4 shadow-sm p-0">
      <table className="table table-light table-striped">
        <thead>
          <tr>
            <th>
              <div className="d-flex">
                <div style={{marginRight: "5px", fontWeight: "bold"}}>Name</div>
                <div className="bi bi-sort-alpha-down" onClick={() => handleSort("name")}></div>
              </div>
            </th>
            <th>
              <div className="d-flex">
                <div style={{marginRight: "5px", fontWeight: "bold"}}>Conference</div>
                <div className="bi bi-sort-alpha-down" onClick={() => handleSort("conference")}></div>
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          {sortedAttendees.map(attendee => {
            return (
              <tr key={attendee.href}>
                <td>{attendee.name}</td>
                <td>{attendee.conference}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}
