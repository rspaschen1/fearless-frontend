import React, { useEffect, useState } from "react"

function ConferenceForm() {
    const [locations, setLocations] = useState([])

    const [name, setName] = useState('')
    const [startDate, setStartDate] = useState('')
    const [endDate, setEndDate] = useState('')
    const [presentations, setPresentations] = useState('')
    const [attendees, setAttendees] = useState('')
    const [description, setDescription] = useState('')
    const [location, setLocation] = useState('')

    const handleNameChange = (event) => {
        const name = event.target.value
        setName(name)
    }
    const handleStartDateChange = (event) => {
        const start = event.target.value
        setStartDate(start)
    }
    const handleEndDateChange = (event) => {
        const end = event.target.value
        setEndDate(end)
    }
    const handlePresentationChange = (event) => {
        const presentation = event.target.value
        setPresentations(presentation)
    }
    const handleAttendeeChange = (event) => {
        const attendee = event.target.value
        setAttendees(attendee)
    }
    const handleDescriptionChange = (event) => {
        const description = event.target.value
        setDescription(description)
    }
    const handleLocationChange = (event) => {
        const location = event.target.value
        setLocation(location)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()
        const data = {}

        data.name = name
        data.starts = startDate
        data.ends = endDate
        data.max_presentations = presentations
        data.max_attendees = attendees
        data.description = description
        data.location = location

        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers:{
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(conferenceUrl, fetchConfig)

        if (response.ok) {
            const newConference = await response.json()

            setName('')
            setStartDate('')
            setEndDate('')
            setPresentations('')
            setAttendees('')
            setDescription('')
            setLocation('')
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <div className="container">
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new conference</h1>
                        <form onSubmit={handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleStartDateChange} value={startDate} placeholder="YYYY-MM-DD" required type="date" name="starts" id="starts" className="form-control"/>
                            <label htmlFor="starts">Starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleEndDateChange} value={endDate} placeholder="YYYY-MM-DD" required type="date" name="ends" id="ends" className="form-control"/>
                            <label htmlFor="ends">Ends</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePresentationChange} value={presentations} placeholder="1000" required type="number" name="max_presentations" id="max_presentations" className="form-control"/>
                            <label htmlFor="max_presentations">Max Presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleAttendeeChange} value={attendees} placeholder="10000" required type="number" name="max_attendees" id="max_attendees" className="form-control"/>
                            <label htmlFor="max_attendees">Max Attendees</label>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleLocationChange} value={location} required name="location" id="location" className="form-select">
                            <option value="">Choose a location</option>
                            {locations.map(location => {
                                return (
                                    <option key={location.name} value={location.name}>{location.name}</option>
                                )
                            })}
                            </select>
                        </div>
                        <div className="form mb-3">
                            <label htmlFor="description" className="form-label">Description:</label>
                            <textarea onChange={handleDescriptionChange} value={description} required name="description" id="description" rows="3" className="form-control"></textarea>
                        </div>
                        <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ConferenceForm
